#!/bin/sh
# bash script to build Dark oCCult for printing

# GNU All-Permissive License
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

cat > style/rego.xml << EOF

    <fop version="1.0">
<renderers>
  <renderer mime="application/pdf">
     <fonts>
        <directory recursive="true">$HOME/.local/share/fonts</directory>
        <auto-detect/>
     </fonts>
  </renderer>
</renderers>
</fop>
EOF


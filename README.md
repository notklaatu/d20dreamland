# Dreamlands Realm for d20

The novella **The Dream-Quest for the Unknown Kadath** is a fantasy story by HP Lovecraft, but also a tour through a strange and captivating fantasy world called "the dreamlands" or "the six kingdoms".

This is a minimalist campaign setting based on Lovecraft's work for d20 games like D&D and Pathfinder. It adds nothing to Lovecraft's source text, and in fact seeks to stay as true to the source text so that you yourself may add flavour of your own.

This booklet gathers information scattered throughout the novella and organizes it into logical non-narrative sections for easier reference.

Some suggested stats for important creatures are provided throughout. Lovecraft has had a huge impact on RPG as it is, though, so many existing [bestiaries](https://koboldpress.com/tome-of-beasts) and [monster manuals](https://www.froggodgames.com/fifth-edition-foes) already contain creatures straight out of this story. Pick them up and support your friendly game developers!

This booklet does not contain a pantheon, but I have published a d20 treatment of [The Gods of Pegana](http://www.drivethrurpg.com/product/232620) and those gods fit rather nicely into a Lovecraft world, and fit better into traditional D&D alignments than a pantheon of pure-Lovecraftian gods ever could.  


## Building this document from source code

You don't need to build this document yourself, if you don't want to. It will be available online once it is finished.

But if you really want to build it from source, you can, because it is open source.

This has only been tested on Linux and BSD, but as long as you know Docbook, you should be able to use this with only modest changes to the GNUmakefile.
      
*Assuming you're using Linux or BSD, the required software is probably available from your software repository or ports tree.*

Requirements:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to overcome render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) or [stapler](https://pypi.python.org/pypi/stapler/)
* [Image Magick](http://imagemagick.org/script/index.php)

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)


### Setup

The font **TeX Gyre Bonum** does not function properly because it's OTF rather than TTF.

I have converted the source OTF to TTF for you. They're included with this repository.

Place them in your ``$HOME/.local/share/fonts/t/`` directory so Docbook can detect them.


### GNUmakefile

The GNUmakefile builds a PDF, the standard delivery format for most indie RPG adventures. It also builds EPUB, the *better* delivery format for indie RPG adventures.

You can edit inline parameters to suit your needs.

Notably:

* paper size is A4 by default
* license is set to ``no`` by default, which renders the standard *for personal use only* footer. You can alternately set it to ``sa`` for a Creative Commons BY-SA license message in the footer, instead. The Open Game License only covers mechanics, so your CC license only applies to your *story content*
* the name of the output file is ``example.pdf`` by default

Once you've written your adventure and are ready to build:

    $ make clean pdf

or

    $ make clean epub
    
The output is placed into a directory called ``dist``.


## Bugs

Report any other bugs you find, or even feedback or suggestions, on Gitlab in the issues tab.


## License

It's all open source and free culture. See LICENSE file for details.
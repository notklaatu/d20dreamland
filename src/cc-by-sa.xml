<chapter id="ccbysa">
  <title>Creative Commons Attribution-ShareAlike 4.0 International
  Public License</title>  

  <para>
    By exercising the Licensed Rights (defined below), You accept and
    agree to be bound by the terms and conditions of this Creative
    Commons Attribution-ShareAlike 4.0 International Public License
    (&quot;Public License&quot;). To the extent this Public License may
    be interpreted as a contract, You are granted the Licensed Rights in
    consideration of Your acceptance of these terms and conditions, and
    the Licensor grants You such rights in consideration of benefits the
    Licensor receives from making the Licensed Material available under
    these terms and conditions.
  </para>
  <para>
    <emphasis role="bold">Section 1 – Definitions.</emphasis>
  </para>
  <orderedlist numeration="arabic" spacing="compact">
    <listitem>
      <para>
        <emphasis role="bold">Adapted Material</emphasis> means
        material subject to Copyright and Similar Rights that is derived
        from or based upon the Licensed Material and in which the
        Licensed Material is translated, altered, arranged, transformed,
        or otherwise modified in a manner requiring permission under the
        Copyright and Similar Rights held by the Licensor. For purposes
        of this Public License, where the Licensed Material is a musical
        work, performance, or sound recording, Adapted Material is
        always produced where the Licensed Material is synched in timed
        relation with a moving image.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Adapter's License</emphasis> means the
        license You apply to Your Copyright and Similar Rights in Your
        contributions to Adapted Material in accordance with the terms
        and conditions of this Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">BY-SA Compatible License</emphasis>
        means a license listed at
        <ulink url="http://creativecommons.org/compatiblelicenses">creativecommons.org/compatiblelicenses</ulink>,
        approved by Creative Commons as essentially the equivalent of
        this Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Copyright and Similar Rights</emphasis>
        means copyright and/or similar rights closely related to
        copyright including, without limitation, performance, broadcast,
        sound recording, and Sui Generis Database Rights, without regard
        to how the rights are labeled or categorized. For purposes of
        this Public License, the rights specified in Section
        <link linkend="s2b">2(b)(1)-(2)</link> are not Copyright and
        Similar Rights.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Effective Technological
        Measures</emphasis> means those measures that, in the absence of
        proper authority, may not be circumvented under laws fulfilling
        obligations under Article 11 of the WIPO Copyright Treaty
        adopted on December 20, 1996, and/or similar international
        agreements.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Exceptions and Limitations</emphasis>
        means fair use, fair dealing, and/or any other exception or
        limitation to Copyright and Similar Rights that applies to Your
        use of the Licensed Material.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">License Elements</emphasis> means the
        license attributes listed in the name of a Creative Commons
        Public License. The License Elements of this Public License are
        Attribution and ShareAlike.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Licensed Material</emphasis> means the
        artistic or literary work, database, or other material to which
        the Licensor applied this Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Licensed Rights</emphasis> means the
        rights granted to You subject to the terms and conditions of
        this Public License, which are limited to all Copyright and
        Similar Rights that apply to Your use of the Licensed Material
        and that the Licensor has authority to license.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Licensor</emphasis> means the
        individual(s) or entity(ies) granting rights under this Public
        License.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Share</emphasis> means to provide
        material to the public by any means or process that requires
        permission under the Licensed Rights, such as reproduction,
        public display, public performance, distribution, dissemination,
        communication, or importation, and to make material available to
        the public including in ways that members of the public may
        access the material from a place and at a time individually
        chosen by them.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Sui Generis Database Rights</emphasis>
        means rights other than copyright resulting from Directive
        96/9/EC of the European Parliament and of the Council of 11
        March 1996 on the legal protection of databases, as amended
        and/or succeeded, as well as other essentially equivalent rights
        anywhere in the world.
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">You</emphasis> means the individual or
        entity exercising the Licensed Rights under this Public License.
        <emphasis role="bold">Your</emphasis> has a corresponding
        meaning.
      </para>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 2 – Scope.</emphasis>
  </para>
  <orderedlist numeration="arabic">
    <listitem>
      <para>
        <emphasis role="bold">License grant</emphasis>.
      </para>
      <orderedlist numeration="arabic" spacing="compact">
        <listitem>
          <para>
            Subject to the terms and conditions of this Public License,
            the Licensor hereby grants You a worldwide, royalty-free,
            non-sublicensable, non-exclusive, irrevocable license to
            exercise the Licensed Rights in the Licensed Material to:
          </para>
          <orderedlist numeration="arabic" spacing="compact">
            <listitem>
              <para>
                reproduce and Share the Licensed Material, in whole or
                in part; and
              </para>
            </listitem>
            <listitem>
              <para>
                produce, reproduce, and Share Adapted Material.
              </para>
            </listitem>
          </orderedlist>
        </listitem>
        <listitem>
          <para>
            Exceptions and Limitations. For the avoidance of doubt,
            where Exceptions and Limitations apply to Your use, this
            Public License does not apply, and You do not need to comply
            with its terms and conditions.
          </para>
        </listitem>
        <listitem>
          <para>
            Term. The term of this Public License is specified in
            Section <link linkend="s6a">6(a)</link>.
          </para>
        </listitem>
        <listitem>
          <para>
            Media and formats; technical modifications allowed. The
            Licensor authorizes You to exercise the Licensed Rights in
            all media and formats whether now known or hereafter
            created, and to make technical modifications necessary to do
            so. The Licensor waives and/or agrees not to assert any
            right or authority to forbid You from making technical
            modifications necessary to exercise the Licensed Rights,
            including technical modifications necessary to circumvent
            Effective Technological Measures. For purposes of this
            Public License, simply making modifications authorized by
            this Section <link linkend="s2a4">2(a)(4)</link> never
            produces Adapted Material.
          </para>
        </listitem>
        <listitem>
          <para>
            Downstream recipients.
          </para>
          <orderedlist numeration="arabic" spacing="compact">
            <listitem>
              <para>
                Offer from the Licensor – Licensed Material. Every
                recipient of the Licensed Material automatically
                receives an offer from the Licensor to exercise the
                Licensed Rights under the terms and conditions of this
                Public License.
              </para>
            </listitem>
            <listitem>
              <para>
                Additional offer from the Licensor – Adapted Material.
                Every recipient of Adapted Material from You
                automatically receives an offer from the Licensor to
                exercise the Licensed Rights in the Adapted Material
                under the conditions of the Adapter’s License You apply.
              </para>
            </listitem>
            <listitem>
              <para>
                No downstream restrictions. You may not offer or impose
                any additional or different terms or conditions on, or
                apply any Effective Technological Measures to, the
                Licensed Material if doing so restricts exercise of the
                Licensed Rights by any recipient of the Licensed
                Material.
              </para>
            </listitem>
          </orderedlist>
        </listitem>
        <listitem>
          <para>
            No endorsement. Nothing in this Public License constitutes
            or may be construed as permission to assert or imply that
            You are, or that Your use of the Licensed Material is,
            connected with, or sponsored, endorsed, or granted official
            status by, the Licensor or others designated to receive
            attribution as provided in Section
            <link linkend="s3a1Ai">3(a)(1)(A)(i)</link>.
          </para>
        </listitem>
      </orderedlist>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">Other rights</emphasis>.
      </para>
      <orderedlist numeration="arabic" spacing="compact">
        <listitem>
          <para>
            Moral rights, such as the right of integrity, are not
            licensed under this Public License, nor are publicity,
            privacy, and/or other similar personality rights; however,
            to the extent possible, the Licensor waives and/or agrees
            not to assert any such rights held by the Licensor to the
            limited extent necessary to allow You to exercise the
            Licensed Rights, but not otherwise.
          </para>
        </listitem>
        <listitem>
          <para>
            Patent and trademark rights are not licensed under this
            Public License.
          </para>
        </listitem>
        <listitem>
          <para>
            To the extent possible, the Licensor waives any right to
            collect royalties from You for the exercise of the Licensed
            Rights, whether directly or through a collecting society
            under any voluntary or waivable statutory or compulsory
            licensing scheme. In all other cases the Licensor expressly
            reserves any right to collect such royalties.
          </para>
        </listitem>
      </orderedlist>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 3 – License Conditions.</emphasis>
  </para>
  <para>
    Your exercise of the Licensed Rights is expressly made subject to
    the following conditions.
  </para>
  <orderedlist numeration="arabic">
    <listitem>
      <para>
        <emphasis role="bold">Attribution</emphasis>.
      </para>
      <orderedlist numeration="arabic">
        <listitem>
          <para>
            If You Share the Licensed Material (including in modified
            form), You must:
          </para>
          <orderedlist numeration="arabic" spacing="compact">
            <listitem>
              <para>
                retain the following if it is supplied by the Licensor
                with the Licensed Material:
              </para>
              <orderedlist numeration="arabic" spacing="compact">
                <listitem>
                  <para>
                    identification of the creator(s) of the Licensed
                    Material and any others designated to receive
                    attribution, in any reasonable manner requested by
                    the Licensor (including by pseudonym if designated);
                  </para>
                </listitem>
                <listitem>
                  <para>
                    a copyright notice;
                  </para>
                </listitem>
                <listitem>
                  <para>
                    a notice that refers to this Public License;
                  </para>
                </listitem>
                <listitem>
                  <para>
                    a notice that refers to the disclaimer of
                    warranties;
                  </para>
                </listitem>
                <listitem>
                  <para>
                    a URI or hyperlink to the Licensed Material to the
                    extent reasonably practicable;
                  </para>
                </listitem>
              </orderedlist>
            </listitem>
            <listitem>
              <para>
                indicate if You modified the Licensed Material and
                retain an indication of any previous modifications; and
              </para>
            </listitem>
            <listitem>
              <para>
                indicate the Licensed Material is licensed under this
                Public License, and include the text of, or the URI or
                hyperlink to, this Public License.
              </para>
            </listitem>
          </orderedlist>
        </listitem>
        <listitem>
          <para>
            You may satisfy the conditions in Section
            <link linkend="s3a1">3(a)(1)</link> in any reasonable manner
            based on the medium, means, and context in which You Share
            the Licensed Material. For example, it may be reasonable to
            satisfy the conditions by providing a URI or hyperlink to a
            resource that includes the required information.
          </para>
        </listitem>
        <listitem>
          <para>
            If requested by the Licensor, You must remove any of the
            information required by Section
            <link linkend="s3a1A">3(a)(1)(A)</link> to the extent
            reasonably practicable.
          </para>
        </listitem>
      </orderedlist>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">ShareAlike</emphasis>.
      </para>
      <para>
        In addition to the conditions in Section
        <link linkend="s3a">3(a)</link>, if You Share Adapted Material
        You produce, the following conditions also apply.
      </para>
      <orderedlist numeration="arabic" spacing="compact">
        <listitem>
          <para>
            The Adapter’s License You apply must be a Creative Commons
            license with the same License Elements, this version or
            later, or a BY-SA Compatible License.
          </para>
        </listitem>
        <listitem>
          <para>
            You must include the text of, or the URI or hyperlink to,
            the Adapter's License You apply. You may satisfy this
            condition in any reasonable manner based on the medium,
            means, and context in which You Share Adapted Material.
          </para>
        </listitem>
        <listitem>
          <para>
            You may not offer or impose any additional or different
            terms or conditions on, or apply any Effective Technological
            Measures to, Adapted Material that restrict exercise of the
            rights granted under the Adapter's License You apply.
          </para>
        </listitem>
      </orderedlist>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 4 – Sui Generis Database
    Rights.</emphasis>
  </para>
  <para>
    Where the Licensed Rights include Sui Generis Database Rights that
    apply to Your use of the Licensed Material:
  </para>
  <orderedlist numeration="arabic" spacing="compact">
    <listitem>
      <para>
        for the avoidance of doubt, Section
        <link linkend="s2a1">2(a)(1)</link> grants You the right to
        extract, reuse, reproduce, and Share all or a substantial
        portion of the contents of the database;
      </para>
    </listitem>
    <listitem>
      <para>
        if You include all or a substantial portion of the database
        contents in a database in which You have Sui Generis Database
        Rights, then the database in which You have Sui Generis Database
        Rights (but not its individual contents) is Adapted Material,
        including for purposes of Section
        <link linkend="s3b">3(b)</link>; and
      </para>
    </listitem>
    <listitem>
      <para>
        You must comply with the conditions in Section
        <link linkend="s3a">3(a)</link> if You Share all or a
        substantial portion of the contents of the database.
      </para>
    </listitem>
  </orderedlist>
  <para>
    For the avoidance of doubt, this Section <link linkend="s4">4</link>
    supplements and does not replace Your obligations under this Public
    License where the Licensed Rights include other Copyright and
    Similar Rights.
  </para>
  <para>
    <emphasis role="bold">Section 5 – Disclaimer of Warranties and
    Limitation of Liability.</emphasis>
  </para>
  <orderedlist numeration="arabic">
    <listitem>
      <para>
        <emphasis role="bold">Unless otherwise separately undertaken
        by the Licensor, to the extent possible, the Licensor offers the
        Licensed Material as-is and as-available, and makes no
        representations or warranties of any kind concerning the
        Licensed Material, whether express, implied, statutory, or
        other. This includes, without limitation, warranties of title,
        merchantability, fitness for a particular purpose,
        non-infringement, absence of latent or other defects, accuracy,
        or the presence or absence of errors, whether or not known or
        discoverable. Where disclaimers of warranties are not allowed in
        full or in part, this disclaimer may not apply to
        You.</emphasis>
      </para>
    </listitem>
    <listitem>
      <para>
        <emphasis role="bold">To the extent possible, in no event will
        the Licensor be liable to You on any legal theory (including,
        without limitation, negligence) or otherwise for any direct,
        special, indirect, incidental, consequential, punitive,
        exemplary, or other losses, costs, expenses, or damages arising
        out of this Public License or use of the Licensed Material, even
        if the Licensor has been advised of the possibility of such
        losses, costs, expenses, or damages. Where a limitation of
        liability is not allowed in full or in part, this limitation may
        not apply to You.</emphasis>
      </para>
    </listitem>
    <listitem>
      <para>
        The disclaimer of warranties and limitation of liability
        provided above shall be interpreted in a manner that, to the
        extent possible, most closely approximates an absolute
        disclaimer and waiver of all liability.
      </para>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 6 – Term and Termination.</emphasis>
  </para>
  <orderedlist numeration="arabic">
    <listitem>
      <para>
        This Public License applies for the term of the Copyright and
        Similar Rights licensed here. However, if You fail to comply
        with this Public License, then Your rights under this Public
        License terminate automatically.
      </para>
    </listitem>
    <listitem>
      <para>
        Where Your right to use the Licensed Material has terminated
        under Section <link linkend="s6a">6(a)</link>, it reinstates:
      </para>
      <orderedlist numeration="arabic" spacing="compact">
        <listitem>
          <para>
            automatically as of the date the violation is cured,
            provided it is cured within 30 days of Your discovery of the
            violation; or
          </para>
        </listitem>
        <listitem>
          <para>
            upon express reinstatement by the Licensor.
          </para>
        </listitem>
      </orderedlist>
      <para>
        For the avoidance of doubt, this Section
        <link linkend="s6b">6(b)</link> does not affect any right the
        Licensor may have to seek remedies for Your violations of this
        Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        For the avoidance of doubt, the Licensor may also offer the
        Licensed Material under separate terms or conditions or stop
        distributing the Licensed Material at any time; however, doing
        so will not terminate this Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        Sections <link linkend="s1">1</link>,
        <link linkend="s5">5</link>, <link linkend="s6">6</link>,
        <link linkend="s7">7</link>, and <link linkend="s8">8</link>
        survive termination of this Public License.
      </para>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 7 – Other Terms and
    Conditions.</emphasis>
  </para>
  <orderedlist numeration="arabic" spacing="compact">
    <listitem>
      <para>
        The Licensor shall not be bound by any additional or different
        terms or conditions communicated by You unless expressly agreed.
      </para>
    </listitem>
    <listitem>
      <para>
        Any arrangements, understandings, or agreements regarding the
        Licensed Material not stated herein are separate from and
        independent of the terms and conditions of this Public License.
      </para>
    </listitem>
  </orderedlist>
  <para>
    <emphasis role="bold">Section 8 – Interpretation.</emphasis>
  </para>
  <orderedlist numeration="arabic" spacing="compact">
    <listitem>
      <para>
        For the avoidance of doubt, this Public License does not, and
        shall not be interpreted to, reduce, limit, restrict, or impose
        conditions on any use of the Licensed Material that could
        lawfully be made without permission under this Public License.
      </para>
    </listitem>
    <listitem>
      <para>
        To the extent possible, if any provision of this Public License
        is deemed unenforceable, it shall be automatically reformed to
        the minimum extent necessary to make it enforceable. If the
        provision cannot be reformed, it shall be severed from this
        Public License without affecting the enforceability of the
        remaining terms and conditions.
      </para>
    </listitem>
    <listitem>
      <para>
        No term or condition of this Public License will be waived and
        no failure to comply consented to unless expressly agreed to by
        the Licensor.
      </para>
    </listitem>
    <listitem>
      <para>
        Nothing in this Public License constitutes or may be interpreted
        as a limitation upon, or waiver of, any privileges and
        immunities that apply to the Licensor or You, including from the
        legal processes of any jurisdiction or authority.
      </para>
    </listitem>
  </orderedlist>
<para>Creative Commons is not a party to its public licenses. Notwithstanding, Creative Commons may elect to apply one of its public licenses to material it publishes and in those instances will be considered the “Licensor.” The text of the Creative Commons public licenses is dedicated to the public domain under the <ulink url="http://creativecommons.org/publicdomain/zero/1.0/legalcode">CC0 Public Domain Dedication</ulink>. Except for the limited purpose of indicating that material is shared under a Creative Commons public license or as otherwise permitted by the Creative Commons policies published at <ulink url="http://creativecommons.org/policies">creativecommons.org/policies</ulink>, Creative Commons does not authorize the use of the trademark “Creative Commons” or any other trademark or logo of Creative Commons without its prior written consent including, without limitation, in connection with any unauthorized modifications to any of its public licenses or any other arrangements, understandings, or agreements concerning use of licensed material. For the avoidance of doubt, this paragraph does not form part of the public licenses.

Creative Commons may be contacted at <ulink url="http://creativecommons.org/">creativecommons.org</ulink>.</para>
</chapter>

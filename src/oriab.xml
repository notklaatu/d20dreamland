<chapter id="oriab">
  <title>Oriab</title>
      <para>
	A large island located in the Southern Sea, about 10 days from
	Dylath-Leen by boat. It is famous for the
	mountain Ngranek, upon which it is said that the gods
	themselves, when they lived on Earth, danced by
	moonlight. Today there is a great image of the gods, carved
	upon solid rock, on the mountain Ngranek.
      </para>

      <para>
	 Oriab is famous for a fragrant resin gathered from its inner
	 groves, for delicate pottery baked by artisans of Baharna,
	 and intricate figures carved from mount Ngranek's ancient
	 lava.
      </para>

      <para>
	An ancient tradition that is carried on even today is
	image-making. Little is know of the art, but lava-gatherers
	and hill people know it well, and there may be old magick
	involved in the process.
      </para>
      
      <para>
	Approaching the island from the East, the mountain Ngranek is
	visible. It is buffeted on the far side side by lava and steep
	craggy cliffs, which popular legend holds as the work of angry
	elder gods.
      </para>

    <section id="baharna">
      <title>Baharna</title>
      <para>
	A port town on Oriab. It is approximately two days from
	Baharna to Ngranek by zebra.
      </para>
      <para>
	The wharves of Baharna are of porphyry, and the city rises in
	great stone terraces behind them, having streets of steps that
	are frequently arched over by buildings and the bridges
	between buildings. There is a great canal under the whole
	city, a tunnel with granite gates that leads to the inland
	lake of Yath. On the opposite shore of Yath are vast
	clay-brick ruins of a primal city whose name is not
	remembered.
      </para>

      <para>
	Twin beacons, Thon and Thal, gleam to welcome incoming ships
	into the port of Baharna,
	and in the night, all the windows of Baharna's terraces
	glitter and glimmer along with the stars until the steep and
	climbing seaport becomes one glittering constellation hung
	between the stars of heaven and the reflections of those stars
	in the still harbour.
      </para>
    </section>
    
      <section id="mitre">
	<title>The Mitre Tavern</title>

	<para>
	 The original part of Baharna is built of brick and resembles
	 the ruins of Yath's farther shore. The old town can be
	 reached through back alleys and forgotten streets, and here
	 there is an old tavern. The source text provides no name, but
	 drawing upon one of Lovecraft&#39;s favourite terms in the
	 source text, it may be called the
	 <emphasis>Mitre Tavern</emphasis>.
      </para>

      <para>
	The keeper of the tavern was a very old man, and had heard so
	many legends that he was a great help. He even took Carter to
	an upper room in that ancient house and shewed him a crude
	picture which a traveller had scratched on the clay wall in
	the old days when men were bolder and less reluctant to visit
	Ngranek's higher slopes. The old tavern-keeper's
	great-grandfather had heard from his great-grandfather that
	the traveller who scratched that picture had climbed Ngranek
	and seen the carven face, here drawing it for others to
	behold, but Carter had very great doubts, since the large
	rough features on the wall were hasty and careless, and wholly
	overshadowed by a crowd of little companion shapes in the
	worst possible taste, with horns and wings and claws and
	curling tails.
      </para>
    </section> <!-- close mitre-->

    <section id="ngranek">
      <title>Ngranek</title>

      <para>
	Ngranek is inland of Baharna. Travelling along the road by
	Yath's shore, the inland region features rolling hills and
	pleasant orchards, with tidy little stone farmhouses. These
	give way to the nameless ancient ruins on Yath's farther
	shore. The ruins are said to be haunted or in some way
	dangerous, so travellers are discouraged from camping there.
      </para>

      <para>
	Uphill through wilder and partly wooded country are the huts
	of charcoal-burners and camps of resin gatherers. The air in
	this region is fragrant with balsam,
and the magah birds sing blithely as they flash their seven
colours in the sun.
      </para>

      <para>
	A local wild beast, the terrible amphibious voonith, is often
	heard howling distantly in the night in this region. While
	horrific, vooniths do not dare approach the slope of Ngranek
	itself.
      </para>
      
      <tip id="stat-voonith">
  <!-- TODO stats for  amphibious voonith   -->
      <title>Voonith &#40;Common&#41;</title>
      <para>XP 400 &#40;CR 4&#41;</para>

      <para>Chaotic evil</para>
      <para>Medium humanoid</para>
      <para>Init +2; Senses low-light vision, scent; Perception +1</para>

      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>AC 14, touch 14, flat-footed 12 (+2 Dex, +2 size)</para>
      <para>hp 4 (1d8)</para>
      <para>Fort +2, Ref +4, Will +1</para>

      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 15 ft., climb 15 ft., swim 15 ft.</para>
      <para>Melee bite +4 (1d3–4)</para>
      <para>Space 2-1/2 ft.; Reach 0 ft.</para>

      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 10, Dex 15, Con 11, Int 10, Wis 16, Cha 8</para>
      <para>Base Atk +0; CMB +0; CMD 6 (10 vs. trip)</para>
      <para><emphasis role="bold">Feats</emphasis> Weapon Finesse</para>
      <para><emphasis role="bold">Skills</emphasis> Climb +10, Stealth +18, Swim +10; Knowledge &#40;history&#41;; Knowledge &#40;planes&#41;; Racial Modifiers&#58; +4 Stealth</para>
      <para><emphasis role="bold">Languages</emphasis> Zoog</para>
      </tip>
	  
      <para>
	Beyond this region is the land of the lava-gatherers. These
	people tend to keep on the move, following the flows of lava
	or ever searching for ancient deposits, and some of their
	abandoned villages litter the lower slopes of Ngranek.
      </para>

      <para>
	At the end of the lower slopes of Ngranek, ash trees, scrub
	oaks, and shrubs
	become sparse, and the bare hideous rock rises
 into the sky to mix with frost and ice and eternal snow.
 In some places there are solid streams of lava, and scoriac heaps
 litter slopes and ledges. The remains of abandoned camps, along with
 makeshift altars built either to propitiate the Great Ones or to ward off what they
dreamed of in Ngranek's high passes and labyrinthine caves, can be
found scattered throughout. 
      </para>

      <para>
	In the higher slopes, where condors nest, the soil becomes meagre with great patches of bare rock cropping
out. Even this gives way, eventually, to bare rock, weathered and
rough with knobs and ledges and
pinnacles that somewhat help a brave adventurer ascend the great
mountain. Even this high, there are signs of desperate lava-gatherers,
in the form of carven footholds or abandoned quarries. 
      </para>

      <para>
	Farther up still there are caves and caverns, forboding and
	pitch black inside. What path there is opens on sheer,
	perpendicular cliffs that are wholly unreachable by the feet
	of man. Even from this height, the far shores of Oriab cannot
	be seen, so large is the island.
      </para>

      <para>
	A strange creature, unseen by man, lurks around the base of
	Ngranek and perhaps along is cliffs. It makes a webbed
	footprint, moves silently, and may or may not have wings. It
	most certainly has fangs capable of piercing flesh and
	draining blood. Such a creature can drain a zebra of its blood
	in less than a night, without the zebra making a sound.
      </para>

      <tip id="stat-voonith">
  <!-- TODO stats for  chupacabra from Frog-god Montsers & Foes    -->
      <title>Chupacabra &#40;Common&#41;</title>
      <para>XP 400 &#40;CR 4&#41;</para>

      <para>Chaotic evil</para>
      <para>Medium humanoid</para>
      <para>Init +2; Senses low-light vision, scent; Perception +1</para>

      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>AC 14, touch 14, flat-footed 12 (+2 Dex, +2 size)</para>
      <para>hp 4 (1d8)</para>
      <para>Fort +2, Ref +4, Will +1</para>

      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 15 ft., climb 15 ft., swim 15 ft.</para>
      <para>Melee bite +4 (1d3–4)</para>
      <para>Space 2-1/2 ft.; Reach 0 ft.</para>

      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 10, Dex 15, Con 11, Int 10, Wis 16, Cha 8</para>
      <para>Base Atk +0; CMB +0; CMD 6 (10 vs. trip)</para>
      <para><emphasis role="bold">Feats</emphasis> Weapon Finesse</para>
      <para><emphasis role="bold">Skills</emphasis> Climb +10, Stealth +18, Swim +10; Knowledge &#40;history&#41;; Knowledge &#40;planes&#41;; Racial Modifiers&#58; +4 Stealth</para>
      <para><emphasis role="bold">Languages</emphasis> Zoog</para>
      </tip>
	
      
      <para>
	Higher still, visible only after scaling a nearly impossible
	incline, is a carving in the side of Ngranek of the polished
	features of a god, stern and terrible, it is the face of a god chiselled by the hands of
the gods, and it looks down haughtily and majestically upon anyone who
finds it. It possesses long, narrow eyes and long-lobed ears,
a thin nose, and a pointed chin. It shines as if on fire in the
sunlight.
      </para>
      <para>
	It is said that the great face carven in the side of Ngranek
	is akin to the people of Celephais, in Ooth-Nargai, beyond the
	Tanarian Hills, for the people of Celephais are rumoured, by
	some, to have descended from the gods.
      </para>

      <para>
	Unfortunately, this peak is under the protection of
	night-gaunts, who serve the archaic god Nodens, and love
	nothing more than to bring prey down, through the caves of
	Ngranek, to the Great Abyss, the underworld of the dreamlands.
      </para>

      <para>
      Ngranek is protected by the night-gaunts, a creature with bat
      wings, inward-curving horns, and black, oily, whale-like
      skin. Their flight makes almost no sound, and neither do they,
      because they have no faces. They do have prehensile paws and
      barbed tails. They are able to pluck their prey from the ground
      and fly away with them, delivering them to the underworld or
      dropping them into the sharp craggy rocks below Ngranek.
    </para>

    <tip id="stat-gaunt">
  <!-- TODO stats for night-gaunts  -->
      <title>Night-gaunt &#91;Va-gar&#93; &#40;Common&#41;</title>
      <para>XP 400 &#40;CR 4&#41;</para>

      <para>Chaotic evil</para>
      <para>Medium humanoid</para>
      <para>Init +2; Senses low-light vision, scent; Perception +1</para>

      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>AC 14, touch 14, flat-footed 12 (+2 Dex, +2 size)</para>
      <para>hp 4 (1d8)</para>
      <para>Fort +2, Ref +4, Will +1</para>

      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 15 ft., climb 15 ft., swim 15 ft.</para>
      <para>Melee bite +4 (1d3–4)</para>
      <para>Space 2-1/2 ft.; Reach 0 ft.</para>

      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 10, Dex 15, Con 11, Int 10, Wis 16, Cha 8</para>
      <para>Base Atk +0; CMB +0; CMD 6 (10 vs. trip)</para>
      <para><emphasis role="bold">Feats</emphasis> Weapon Finesse</para>
      <para><emphasis role="bold">Skills</emphasis> Climb +10, Stealth +18, Swim +10; Knowledge &#40;history&#41;; Knowledge &#40;planes&#41;; Racial Modifiers&#58; +4 Stealth</para>
      <para><emphasis role="bold">Languages</emphasis> Zoog</para>
  </tip> 

  <para>
    Night-gaunts can only be reasoned with by invoking a password
    known to the ghouls, because night-gaunts are bound by solemn
    treaties with the ghouls. Night-gaunts are allies of ghouls, and
    will fight to the death to aid a ghoul or a friend of a ghoul.
  </para>
</section>

</chapter>

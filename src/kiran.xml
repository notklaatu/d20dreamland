<chapter id="kiran">
  <title>Kiran</title>

  <para>
    Along the northern border of the Enchanted Woods are garden lands
    that lie between the wood and the Cerenerian Sea, and through
    which flows the singing river Oukranos. The sun rises high over
    these gentle slopes of grove and lawn, heightening the colours of
    the thousand flowers that star each knoll and dangle. A haze lies
    upon all this region, so that the land holds a little more of the
    sunlight than other places hold, and a little more of the summer's
    humming music of birds and bees. When adventurers walk through it, as through a faery place, they feel greater joy and wonder than they ever afterward remember.
  </para>
  <para>
    Half a day's walk from the Enchanted Woods are the jasper terraces
    of <emphasis role="bold">Kiran</emphasis>, which slope down to the
    river's edge and bear a temple of loveliness wherein the King of
    Ilek-Vad comes from his far realm on the twilight sea once a year
    in a golden palanquin to pray to the god of Oukranos, who sang to
    him in youth when he dwelt in a cottage by its banks. The temple
    is made entirely of jasper, and it covers an acre of ground with
    its walls and courts, its seven pinnacled towers, and its inner
    shrine where the river enters through hidden channels and the god
    sings softly in the night. Many times the moon hears strange music
    as it shines on those courts and terraces and pinnacles, but
    whether that music be the song of the god or the chant of the
    cryptical priests, none but the King of Ilek-Vad may say; for only
    he had entered the temple or seen the priests.
  </para>

  <para>
    Beyond Kiran are peaceful, thatched cottages, and shrines of
    amiable gods carven from jasper or chrysoberyl. In this region,
    the Oukranos is readily accessible, and you might catch a glipmse
    of iridescent fish swimming past. There are also carnivorous fish
    here, which catch fishing birds by luring them toward the water by
    showing tempting scales in the sun, and then grasping a bird who
    comes down to catch it by the beak and devouring it.
  </para>

  <para>
    A great dark wood lies across the Oukranos, and its trees come
    down clear to the water's edge. Quaint, lumbering buopoths
    sometimes can be spotted coming shyly out of that wood to drink.
  </para>

  <section id="thran">
    <title>Thran</title>

    <para>
    A day out from the Enchanted Woods lies the thousand gilded spires
    of the city of <emphasis role="bold">Thran</emphasis>. Lofty
    beyond belief are the alabaster walls of that incredible city,
    sloping inward toward the top, and wrought in one solid piece by
    what means no man knows. Thran is an ancient city, impossible to
    have built, and yet it stands. It has a hundred gates and two
    hundred turrets, with clustered towers within, white beneath
    golden spires, that are so tall that people on the plains around
    see them soaring into the sky, sometimes shining clear, sometimes
    caught at the top in tangles of cloud and mist, and sometimes
    clouded lower down with their utmost pinnacles blazing free above
    the vapours.
    </para>

    <para>
      Where Thran's gates open on the Oukranos are great wharves of
      marble with ornate galleons of fragrant cedar and calamander
      riding gently at anchor. Strange bearded sailors sit on casks
      and bales emblazoned with the hieroglyphs of far off places.
    </para>

    <para>
      Landward beyond the walls of Thran lies farm country, where
      small white cottages dream between little hills, and narrow
      roads with many stone bridges wind gracefully among streams and
      gardens.
    </para>

    <para>
      The city is guarded by red-robed sentries who admit travellers
      only upon payment of fantastic stories. A traveller must tell
      the sentry three dreams beyond belief, or is denied entry.
    </para>

    <para>
      The gateways into Thran pass through the outer wall, which is so
      thick that the gate is a veritable tunnel. The streets curve and
      undulate their ways deep between heavenward towers, leading to
      bazaars where the wares of the ornate galleons are sold. All
      throughout the city, lights shine through grated and balconied
      windows, the sound of lutes and pipes steal timidly from inner
      courts where marble fountains bubble.
    </para>

    <para>
      The lower city leads, through darker streets, to old sea
      taverns, where captains and seamen gather. At such
      establishments, travellers may purchase passage on great green
      galleons, along the Oukranos to the Cerenerian Sea, toward
      Celephaïs in the land of Ooth-Nargai.
    </para>
  </section>
</chapter>
